'use client';

import Alert from '@mui/material/Alert';
import Box from '@mui/material/Box';
import Grid from '@mui/material/Grid';

import { Typography } from '@mui/material';
import { FormikErrors, FormikProps, FormikProvider, useFormik } from 'formik';
import { ChangeEvent, ReactElement, RefObject, useEffect } from 'react';
import { ActionButton } from './fields';
import { DateField, GenericDropdown, GenericTextField } from './fields';

import type { GenericFormItem } from '../utils';

export type GenericFormItemProps = GenericFormItem & {
  /** Callback function to handle changes to the field at the form level */
  handleChange?: (e: ChangeEvent<any>) => void;
  /** Same as `handleChange`, but for a `GenericDropdown` (MUI Autocomplete) */
  setValue?: (
    field: string,
    value: any,
    shouldValidate?: boolean,
  ) => Promise<void> | Promise<FormikErrors<any>>;
  /** Whether or not this field should be disabled */
  formDisabled?: boolean;
  /** The formik form props */
  formik?: FormikProps<any>;
};

export type GenericFormProps = {
  /** The title of the form, to be displayed above the form */
  title: string;
  /** An array denoting what types of fields and their names should be included in this form */
  fields: Array<GenericFormItem>;
  /** Callback function that performs an action when the form is submitted */
  onSubmit: (values: any) => Promise<void>;
  /** Callback function to handle validation errors */
  handleValidationError?: () => void;
  /** Whether or not this form should be disabled */
  disabled?: boolean;
  /**
   * Submit button configuration. Consists of a title as a `string`, and an icon as a `ReactElement`
   *
   * @remarks
   * If this value is left undefined, there will not be a submit button displayed at the bottom of the form
   */
  submitBtnConfig?: {
    title: string;
    icon: ReactElement;
  };
  /**
   * Configuration for an action button to be placed in the top right of the form. Same schema as `submitBtnConfig`
   *
   * @remarks
   * If this value is left undefined, there will not be a submit button displayed at the bottom of the form
   */
  actionButtonConfig?: {
    title: string;
    icon: ReactElement;
    action: () => Promise<void>;
  };
  /**
   * Ref used for submitting this form from a parent element.
   *
   * @remarks
   * Cannot use this property in tandem with `submitBtnConfig`
   */
  submitRef?: RefObject<HTMLButtonElement>;
  /**
   * Optionally, a schema used to validate all fields in the form. See [yup](https://github.com/jquense/yup)
   *
   * @defaultValue null
   */
  validationSchema?: any;
  /**
   * The width of the form
   *
   * @defaultValue 75%
   */
  width?: string;
  /**
   * The color of the border of the form
   *
   * @defaultValue #000
   */
  borderColor?: string;
  /**
   * An error message to display under the form
   *
   * @defaultValue null
   */
  errorMsg?: string | null;
  /**
   * For fields belonging to this form that don't exist in the fields array, what their initial values should be
   */
  additionalDefaultValues?: { [key: string]: any };
};

/**
 * Returns the proper component for the field type coming from {@link GenericFormItem}.
 *
 * @param {GenericFormItemProps} props
 *
 * @remarks
 * For internal use only
 */
function GenericFormItemComp(props: GenericFormItemProps) {
  const commonProps = {
    fcName: props.fcName,
    type: props.type,
    name: props.name,
    value: props.value,
    required: props.required,
    error: props.error,
    errorMessage: props.errorMessage,
    disabled: props.formDisabled || props.disabled,
    tooltip: props.tooltip,
  };

  if (props.type === 'custom') {
    if (!props.component) {
      throw Error('Must provide "component" as a prop!');
    }

    return props.component;
  } else if (props.type === 'date') {
    return <DateField {...commonProps} formik={props.formik} />;
  } else if (props.type === 'dropdown' && props.options) {
    if (!props.options) {
      throw Error('Must provide "options" as a prop!');
    }

    if (!props.dropdownLabelFunctionPred) {
      throw Error('Must provide "dropdownLabelFunctionPred as a prop!');
    }

    if (!props.optionKey) {
      throw Error('Must provide "optionKey" as a prop!');
    }

    if (!props.optionValueKey) {
      throw Error('Must provide "optionValueKey" as a prop!');
    }

    return (
      <GenericDropdown
        {...commonProps}
        onValueChange={props.setValue}
        options={props.options}
        optionKey={props.optionKey}
        optionValueKey={props.optionValueKey}
        labelFunctionPred={props.dropdownLabelFunctionPred}
        multiple={props.multiple}
        onInputChange={props.onInputChange}
      />
    );
  } else if (props.type === 'text' || props.type === 'number') {
    return (
      <GenericTextField
        {...commonProps}
        onValueChange={props.handleChange}
        inputProps={props.inputProps}
      />
    );
  } else if (props.type === 'multiline') {
    return (
      <GenericTextField
        {...commonProps}
        onValueChange={props.handleChange}
        multiLine={true}
      />
    );
  } else {
    return <></>;
  }
}

type GenericFormHeaderProps = {
  title: string;
  actionButtonConfig?: {
    title: string;
    icon: ReactElement;
    action: () => Promise<void>;
  };
};

function GenericFormHeader(props: GenericFormHeaderProps) {
  const formTitleWidth = props.actionButtonConfig ? 8 : 12;
  const formTitle = (
    <Grid item xs={formTitleWidth}>
      <Typography
        variant="h6"
        sx={{ textAlign: 'left', color: 'primary.main' }}
      >
        {props.title}
      </Typography>
    </Grid>
  );

  if (!props.actionButtonConfig) {
    return <Grid container>{formTitle}</Grid>;
  } else {
    return (
      <Grid container>
        {formTitle}
        <Grid
          container
          item
          xs={4}
          direction="column"
          alignItems="flex-end"
          sx={{ mt: -3 }}
        >
          <ActionButton
            title={props.actionButtonConfig.title}
            startIcon={props.actionButtonConfig.icon}
            onButtonClick={props.actionButtonConfig.action}
          />
        </Grid>
      </Grid>
    );
  }
}

/**
 * Generates a form with specified fields (see {@link GenericFormItem}).
 *
 * @param {GenericFormProps} props
 * @example
 * import { GenericForm } from '@oklahoma-biological-survey/obs-react-lib/nextjs/components';
 * import type { GenericFormItem } from '@oklahoma-biological-survey/obs-react-lib/nextjs/utils/types';
 *
 * export default function TestForm() {
 *   const dropdownOptions = [
 *     'Option 1',
 *     'Option 2'
 *   ];
 *
 *   const fields: Array<GenericFormItem> = [
 *     { fcName: 'field1', name: 'Field 1', type: 'dropdown', options: dropdownOptions, value: 'Option 1 },
 *     { fcName: 'field2', name: 'Field 2', type: 'text', value: '' }
 *   ];
 *
 *   const onSubmit = (values: any) => console.log(values);
 *
 *   return (
 *     <GenericForm
 *       title='Test Form'
 *       fields={fields}
 *       onSubmit={onSubmit}
 *       submitButtonTitle='Submit Test Form'
 *     />
 *   );
 * }
 */
export default function GenericForm(props: GenericFormProps) {
  props = {
    ...props,
    width: !props.width ? '75%' : props.width,
    borderColor: !props.borderColor ? '#000' : props.borderColor,
    errorMsg: !props.errorMsg ? null : props.errorMsg,
    disabled: !props.disabled ? false : props.disabled,
  };

  const defaultValues = props.fields.reduce((map, field) => {
    map[field.fcName] = field.value ? field.value : '';
    return map;
  }, props.additionalDefaultValues || {});

  const formik = useFormik({
    initialValues: defaultValues,
    validationSchema: props.validationSchema,
    onSubmit: (values) => handleSubmit(values),
  });

  // TODO: need to also check for valid status so that the callback is called upon first invalid validation
  useEffect(() => {
    if (!formik.isValid && props.handleValidationError) {
      props.handleValidationError();
    }
  }, [formik.submitCount]);

  const handleSubmit = async (values: any) => {
    await props.onSubmit(values);
    formik.setSubmitting(false);
  };

  return (
    <Box
      sx={{
        border: 2,
        borderColor: props.borderColor,
        borderRadius: '16px',
        p: 3,
        pt: 1.5,
        mt: 2,
        mx: 'auto',
        width: props.width,
      }}
    >
      <GenericFormHeader
        title={props.title}
        actionButtonConfig={props.actionButtonConfig}
      />
      <FormikProvider value={formik}>
        <form onSubmit={formik.handleSubmit}>
          {props.fields.map((field: GenericFormItem, index: number) => (
            <GenericFormItemComp
              key={index}
              fcName={field.fcName}
              name={field.name}
              type={field.type}
              required={field.required}
              value={formik.values[field.fcName]}
              inputProps={field.inputProps}
              disabled={field.disabled}
              error={
                (formik.touched[field.fcName] &&
                  Boolean(formik.errors[field.fcName])) ||
                field.error
              }
              errorMessage={
                (formik.touched[field.fcName] &&
                  (formik.errors[field.fcName] as string)) ||
                field.errorMessage
              }
              handleChange={formik.handleChange}
              options={field.options}
              dropdownLabelFunctionPred={field.dropdownLabelFunctionPred}
              optionKey={field.optionKey}
              optionValueKey={field.optionValueKey}
              setValue={formik.setFieldValue}
              multiple={field.multiple}
              formDisabled={props.disabled}
              component={field.component}
              onInputChange={field.onInputChange}
              formik={formik}
              tooltip={field.tooltip}
            />
          ))}
          {props.submitBtnConfig && !props.submitRef ? (
            <ActionButton
              title={props.submitBtnConfig.title}
              startIcon={props.submitBtnConfig.icon}
              submitting={formik.isSubmitting}
              disabled={formik.isSubmitting || props.disabled}
            />
          ) : (
            <button
              ref={props.submitRef}
              type="submit"
              style={{ display: 'none' }}
            />
          )}
        </form>
      </FormikProvider>
      {props.errorMsg && (
        <Alert severity="error" sx={{ mt: 3 }}>
          {props.errorMsg}
        </Alert>
      )}
    </Box>
  );
}
