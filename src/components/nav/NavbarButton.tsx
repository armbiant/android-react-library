'use client';

import Button from '@mui/material/Button';
import Divider from '@mui/material/Divider';
import ListItemButton from '@mui/material/ListItemButton';
import ListItemText from '@mui/material/ListItemText';
import Link from 'next/link';

import { alpha } from '@mui/material';
import { Route } from '../../utils/types';

type NavbarButtonProps = {
  route: Route;
  currentRoute: string | null;
  getPath: (route: Route, currentRoute: string) => string | null;
};

type NavbarExpandButtonProps = NavbarButtonProps & {
  closeDrawer: () => void;
};

export function NavbarButton(props: NavbarButtonProps) {
  const path = props.getPath(
    props.route,
    props.currentRoute as string,
  ) as string;
  const isActive = path === props.currentRoute;

  return (
    <>
      <Link href={path} style={{ textDecoration: 'none' }}>
        <Button
          className={`${isActive ? 'active' : ''}`}
          key={props.route.name}
          sx={{
            color: '#fff',
            margin: '0 5px',
            ':hover': { bgcolor: alpha('#000', 0.3) },
          }}
        >
          {props.route.name}
        </Button>
      </Link>
      {props.route.divider ? (
        <Divider
          orientation="vertical"
          style={{
            backgroundColor: 'white',
            height: '40px',
            display: 'inline-block',
            width: '2px',
            verticalAlign: 'middle',
          }}
        />
      ) : null}
    </>
  );
}

export function NavbarExpandButton(props: NavbarExpandButtonProps) {
  // TODO: need to check if user is logged in
  const path = props.route.login
    ? '/auth/login'
    : (props.getPath(props.route, props.currentRoute as string) as string);

  return (
    <Link
      href={path}
      onClick={props.closeDrawer}
      style={{ textDecoration: 'none' }}
    >
      <ListItemButton>
        <ListItemText primary={props.route.name} />
      </ListItemButton>
    </Link>
  );
}
