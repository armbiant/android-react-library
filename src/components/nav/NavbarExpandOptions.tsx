'use client';

import ExpandLess from '@mui/icons-material/ExpandLess';
import ExpandMore from '@mui/icons-material/ExpandMore';
import Collapse from '@mui/material/Collapse';
import List from '@mui/material/List';
import ListItem from '@mui/material/ListItem';
import ListItemButton from '@mui/material/ListItemButton';
import ListItemText from '@mui/material/ListItemText';

import { Theme } from '@mui/material';
import { useState } from 'react';
import { isRoute } from '../../utils';
import { Route } from '../../utils/types';
import { CustomLink } from './NavbarDropdown';

type NavbarExpandOptionsProps = {
  routes: Array<Route>;
  index: number | undefined;
  theme: Theme;
};

export function NavbarExpandOptions(props: NavbarExpandOptionsProps) {
  const [open, setOpen] = useState(false);
  const route =
    props.index !== undefined && isRoute(props.routes[props.index])
      ? props.routes[props.index]
      : null;

  const handleClick = () => {
    setOpen(!open);
  };

  if (route) {
    return (
      <>
        <ListItemButton onClick={handleClick}>
          <ListItemText
            primary={route.name}
            key={props.index}
            sx={{ color: props.theme.palette.primary.main }}
          />
          {open ? <ExpandLess /> : <ExpandMore />}
        </ListItemButton>
        <Collapse in={open} timeout="auto" unmountOnExit>
          <List component="div" disablePadding>
            {route.options?.map((option, index) => (
              <ListItem key={index}>
                <CustomLink path={option.paths[0]}>
                  <ListItemButton sx={{ pl: 4 }}>
                    <ListItemText primary={option.name} />
                  </ListItemButton>
                </CustomLink>
              </ListItem>
            ))}
          </List>
        </Collapse>
      </>
    );
  } else {
    return null;
  }
}
