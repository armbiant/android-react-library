import CancelIcon from '@mui/icons-material/Cancel';
import CheckIcon from '@mui/icons-material/Check';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogTitle from '@mui/material/DialogTitle';

import { ActionButton } from './fields';

type ConfirmDialogProps = {
  message: string;
  open: boolean;
  onCancel: () => void;
  onConfirm: () => void;
};

export default function ConfirmDialog(props: ConfirmDialogProps) {
  const handleCancel = () => {
    props.onCancel();
  };

  const handleConfirm = () => {
    props.onConfirm();
  };

  return (
    <Dialog open={props.open}>
      <DialogTitle>Confirm</DialogTitle>
      <DialogContent>{props.message}</DialogContent>
      <DialogActions>
        <ActionButton
          title="Cancel"
          startIcon={<CancelIcon />}
          onButtonClick={handleCancel}
        />
        <ActionButton
          title="Confirm"
          startIcon={<CheckIcon />}
          onButtonClick={handleConfirm}
        />
      </DialogActions>
    </Dialog>
  );
}
