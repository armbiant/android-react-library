'use client';

import Container from '@mui/material/Container';
import LoginForm from './LoginForm';

import { Theme } from '@mui/material';

type LoginProps = {
  loginFn: (credentials: { username: string; password: string }) => void;
  theme: Theme;
};

export default function Login(props: LoginProps) {
  return (
    <Container sx={{ mb: 2 }}>
      <LoginForm
        loginFn={props.loginFn}
        theme={props.theme}
        invalidCredentials={false}
        width="100%"
      />
    </Container>
  );
}
