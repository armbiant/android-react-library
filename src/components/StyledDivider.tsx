'use client';

import Divider from '@mui/material/Divider';

import { Theme } from '@mui/material';

export type StyledDividerProps = {
  /** The width of the divider */
  margin?: number;
  /** The color of the divider */
  theme?: Theme;
};

/**
 * Component that generates a horizontal divider.
 * Based on [MUI Divider.](https://mui.com/material-ui/react-divider/)
 *
 * @remarks
 * This component will be removed in a future release
 *
 * @param {StyledDividerProps} props
 */
export default function StyledDivider(
  props: StyledDividerProps = { margin: 3 },
) {
  const dividerColor = props.theme ? props.theme.palette.primary.main : '#000';

  return (
    <>
      <Divider
        variant="middle"
        sx={{
          margin: props.margin,
          backgroundColor: dividerColor,
          borderBottomWidth: 3,
          opacity: 0.5,
        }}
      />
    </>
  );
}
