'use client';

import Box from '@mui/material/Box';
import Container from '@mui/material/Container';
import LinearProgress from '@mui/material/LinearProgress';

import { useState } from 'react';

export type EsriMapProps = {
  /** The value used to perform an ArcGIS query on. Used in `initializeMap()` function */
  queryValue: string;
  /**
   * The function that performs the construction of the map. [See documentation](https://developers.arcgis.com/javascript/latest/)
   *
   * @param queryValue The value used to perform an ArcGIS query on
   * @param setLoading callback function to hide the indeterminate progress bar
   * @returns void
   */
  initializeMap: (
    queryValue: string,
    setLoading: (loading: boolean) => void,
  ) => void;
  /** The height of the map's container */
  height: number;
};

/**
 * Component that generates and displays a map based on the [ESRI JS API.](https://developers.arcgis.com/javascript/latest/)
 * This will display an indeterminate progress bar above the map that, when the map is loaded, will disappear.
 *
 * @param {EsriMapProps} props
 * @requires esri-loader [NPM](https://www.npmjs.com/package/esri-loader)
 *
 * @example
 * import { EsriMap } from '@oklahoma-biological-survey/obs-react-lib/nextjs/components';
 * import { loadModules, setDefaultOptions } from 'esri-loader';
 *
 * function initializeMap(acode: string, setLoading: (loading: boolean) => void) {
 *   loadModules([
 *     ...,
 *     'esri/Map',
 *     'esri/views/MapView',
 *     'esri/core/reactiveUtils'
 *   ]).then(([
 *     ...,
 *     Map,
 *     MapView,
 *     reactiveUtils
 *   ]) => {
 *     ...
 *     const map = new Map({
 *       layers: ...
 *     });
 *
 *     const view = new MapView({
 *       container: 'mapContainer', # must be 'mapContainer'
 *       map
 *     });
 *
 *     reactiveUtils.when(() => !view.updating, () => setLoading(false));
 *   });
 * }
 *
 * function Map({acode: string}) {
 *   // Load CSS from ESRI JS API
 *   setDefaultOptions({ css: true });
 *
 *   return (
 *     <EsriMap queryValue={acode} initializeMap={initializeMap} height={700} />
 *   );
 * }
 */
export default function EsriMap(props: EsriMapProps) {
  const [loading, setLoading] = useState(true);

  if (loading) {
    props.initializeMap(props.queryValue, setLoading);
  }

  return (
    <Container sx={{ width: '100%', mx: 'auto' }}>
      <Box sx={{ mb: -1.5, height: 4 }}>
        <LinearProgress
          color="primary"
          sx={{ display: loading ? 'block' : 'none' }}
        />
      </Box>
      <div id="mapContainer" style={{ height: props.height }} />
    </Container>
  );
}
