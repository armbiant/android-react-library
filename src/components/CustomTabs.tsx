import Box from '@mui/material/Box';
import Tab from '@mui/material/Tab';
import Tabs from '@mui/material/Tabs';

import { Theme } from '@mui/material';
import { useState } from 'react';
import { CustomTab } from '../utils/types';

type TabPanelProps = {
  children?: React.ReactNode;
  index: number;
  value: number;
};

export type CustomTabsProps = {
  /** A list of tabs to be displayed */
  tabs: Array<CustomTab>;
  /** An optional MUI theme */
  theme?: Theme;
};

function a11yProps(index: number) {
  return {
    id: `tab-${index}`,
    'aria-controls': `tabpanel-${index}`,
  };
}

function TabPanel(props: TabPanelProps) {
  const { children, value, index, ...other } = props;

  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`tabpanel-${index}`}
      aria-labelledby={`tab-${index}`}
      {...other}
    >
      {value === index && <Box sx={{ p: 3 }}>{children}</Box>}
    </div>
  );
}

/**
 * Creates a tabbed component. Allows for a name and icon for each tab, along with a custom component that should be displayed.
 * Based on [MUI Tabs.](https://mui.com/material-ui/react-tabs/)
 *
 * @param {CustomTabsProps} props
 *
 * @example
 * import AbcIcon from '@mui/icons-material/Abc';
 * import AddIcon from '@mui/icons-material/Add';
 *
 * import { CustomTab } from '@oklahoma-biological-survey/obs-react-lib/nextjs/utils/types';
 * import { CustomTabs } from '@oklahoma-biological-survey/obs-react-lib/nextjs/components';
 *
 * function Tab1() {
 *   return <p>Tab 1</p>
 * }
 *
 * function Tab2() {
 *   return <p>Tab 2</p>
 * }
 *
 * export function Tabs() {
 *   const tabs: Array<CustonTab> = [
 *     {
 *       title: 'Tab 1',
 *       icon: <AbcIcon />,
 *       content: <Tab1 />
 *     },
 *     {
 *       title: 'Tab 2',
 *       icon: <AddIcon />,
 *       content: <Tab2 />
 *     }
 *   ];
 *
 *   return (
 *     <CustomTabs tabs={tabs} />
 *   );
 * }
 */
export default function CustomTabs(props: CustomTabsProps) {
  const [tabValue, setTabValue] = useState(0);

  const handleTabChange = (event: React.SyntheticEvent, newValue: number) => {
    setTabValue(newValue);
  };

  return (
    <>
      <Box p={2} pt={0} sx={{ textAlign: 'left', width: '75vw', mx: 'auto' }}>
        <Tabs
          value={tabValue}
          onChange={handleTabChange}
          textColor="primary"
          indicatorColor="secondary"
          variant="scrollable"
        >
          {props.tabs.map((tab: CustomTab, index: number) => (
            <Tab
              key={index}
              icon={tab.icon}
              label={tab.title}
              {...a11yProps(index)}
            />
          ))}
        </Tabs>
      </Box>
      <Box
        sx={{
          textAlign: 'left',
          border: '1px solid',
          borderColor: props.theme?.palette.primary.main,
          width: '75vw',
          mx: 'auto',
        }}
        borderRadius={2}
      >
        {props.tabs.map((tab: CustomTab, index: number) => (
          <TabPanel key={index} value={tabValue} index={index}>
            {tab.content}
          </TabPanel>
        ))}
      </Box>
    </>
  );
}
