'use client';

import Autocomplete from '@mui/material/Autocomplete';
import Chip from '@mui/material/Chip';
import FormControl from '@mui/material/FormControl';
import TextField from '@mui/material/TextField';
import Tooltip from '@mui/material/Tooltip';
import Zoom from '@mui/material/Zoom';

import CircularProgress from '@mui/material/CircularProgress';
import { FormikErrors, useField } from 'formik';
import { isEqual } from 'lodash';
import { ChangeEvent, useState } from 'react';
import { GenericFormItem } from '../../utils';

export type GenericDropdownProps = {
  /** The name of the form control to assign to this field */
  fcName: string;
  /** The name of the field, displayed on the field's label */
  name: string;
  /** The list of options to display in this dropdown */
  options: Array<any>;
  /** The function predicate that should be called to determine the displayed labels */
  labelFunctionPred: (option: any) => string;
  /** The unique key for the option */
  optionKey: string;
  /** The key used to get the value of the option */
  optionValueKey: string;
  /**
   * The type of field
   *
   * @defaultValue 'dropdown'
   */
  type?: GenericFormItem['type'];
  /**
   * The variant this field should be displayed as
   *
   * @defaultValue standard
   */
  variant?: 'standard' | 'outlined';
  /**
   * Callback function to perform an action when the input value changes
   *
   * @defaultValue null
   */
  onInputChange?: (event: React.ChangeEvent<{}>, newValue: any) => void;
  /**
   * Callback function to perform an action when the value changes
   *
   * @defaultValue null
   */
  onValueChange?:
    | ((
        field: string,
        value: any,
        shouldValidate?: boolean,
      ) => Promise<void> | Promise<FormikErrors<any>>)
    | null;
  /**
   * Whether or not this field is required in its form
   *
   * @defaultValue false
   */
  required?: boolean;
  /**
   * The width of the field
   *
   * @defaultValue 100%
   */
  width?: string;
  /**
   * The value of the field
   *
   * @defaultValue ''
   */
  value?: any;
  /**
   * Whether or not multiple selection is allowed
   *
   * @defaultValue false
   */
  multiple?: boolean;
  /**
   * Whether or not this field is disabled
   *
   * @defaultValue false
   */
  disabled?: boolean;
  /**
   * Whether or not this field has an error
   *
   * @defaultValue false
   */
  error?: boolean;
  /**
   * The text of any errors
   *
   * @default null
   */
  errorMessage?: string | null;
  /**
   * Whether or not an asynchronous search is being performed
   */
  searching?: boolean;
  /**
   * The function that performs validations for this specific field
   *
   * @param value The value of the field
   * @returns An error message if the field is invalid, or undefined otherwise
   */
  validateFn?: (value: any) => string | undefined;
  /**
   * Whether or not this field is not part of a form
   *
   * @default false
   */
  standalone?: boolean;
  /**
   * Whether or not to disable the clear button
   *
   * @default false
   */
  disableClear?: boolean;
  /**
   * Optional text to be displayed in a tooltip above the field
   *
   * @default '''
   */
  tooltip?: string;
};

const DEFAULT_PROPS: GenericDropdownProps = {
  fcName: '',
  name: '',
  options: new Array<any>(),
  labelFunctionPred: (option: any) => option as string,
  optionKey: '',
  optionValueKey: '',
  type: 'dropdown',
  variant: 'standard',
  onValueChange: null,
  required: false,
  width: '100%',
  value: null,
  multiple: false,
  disabled: false,
  standalone: false,
  tooltip: '',
};

/**
 * Generates a generic dropdown field.
 * Based on [MUI Autocomplete.](https://mui.com/material-ui/react-autocomplete/)
 *
 * @param {GenericDropdownProps} props
 * @example
 * import { GenericDropdown } from '@oklahoma-biological-survey/obs-react-lib/nextjs/components';
 *
 * export function Dropdown() {
 *   const options = [
 *     'Option 1',
 *     'Option 2',
 *     'Option 3'
 *   ];
 *
 *   return (
 *     <GenericDropdown fcName='test' name='Test Dropdown' options={options} />
 *   );
 * }
 */
export default function GenericDropdown(props: GenericDropdownProps) {
  props = {
    ...props,
    type: !props.type ? DEFAULT_PROPS.type : props.type,
    variant: !props.variant ? DEFAULT_PROPS.variant : props.variant,
    required: !props.required ? DEFAULT_PROPS.required : props.required,
    width: !props.width ? DEFAULT_PROPS.width : props.width,
    multiple: !props.multiple ? DEFAULT_PROPS.multiple : props.multiple,
    disabled: !props.disabled ? DEFAULT_PROPS.disabled : props.disabled,
    value: !props.value ? DEFAULT_PROPS.value : props.value,
    tooltip: !props.tooltip ? DEFAULT_PROPS.tooltip : props.tooltip,
  };

  type ValueType = typeof props.value;

  const [value, setValue] = props.standalone
    ? useState(props.value)
    : [undefined, undefined];
  const [field, meta, helpers] = props.standalone
    ? [undefined, undefined, undefined]
    : useField({
        name: props.fcName,
        value: props.value,
        validate: props.validateFn,
      });

  const handleChange = (event: ChangeEvent, newValue: ValueType | null) => {
    props.standalone ? setValue?.(newValue) : helpers?.setValue(newValue);

    if (props.onValueChange) {
      props.onValueChange(props.fcName, newValue);
    }
  };

  return (
    <Tooltip
      title={props.tooltip}
      placement="right"
      TransitionComponent={Zoom}
      enterDelay={500}
      enterNextDelay={100}
      arrow
    >
      <Autocomplete
        id={`autocomplete-${props.fcName}`}
        multiple={props.multiple}
        disabled={props.disabled}
        disableClearable={props.disableClear}
        value={props.standalone ? value : field?.value}
        onChange={handleChange}
        onInputChange={props.onInputChange}
        options={props.options}
        getOptionDisabled={(option) => option.disabled}
        getOptionLabel={props.labelFunctionPred}
        isOptionEqualToValue={(option, value) => {
          // Accept null and empty string
          return isEqual(value, option) || value === null || value === '';
        }}
        renderOption={(optionProps, option) => {
          return (
            <li {...optionProps} key={option[props.optionKey]}>
              {option[props.optionValueKey]}
            </li>
          );
        }}
        renderTags={(tagValue, getTagProps) => {
          return tagValue.map((option, index) => (
            <Chip
              {...getTagProps({ index })}
              label={option[props.optionValueKey]}
              key={index}
            />
          ));
        }}
        renderInput={(params) => (
          <FormControl
            variant={props.variant}
            sx={{ mt: 1.5, width: props.width }}
            required={props.required}
          >
            <TextField
              {...params}
              id={`dropdown-${props.fcName}`}
              name={props.fcName}
              label={props.name}
              variant={props.variant}
              error={props.error}
              helperText={props.errorMessage}
              required={props.required}
              InputProps={{
                ...params.InputProps,
                endAdornment: (
                  <>
                    {props.searching ? (
                      <CircularProgress color="primary" size={20} />
                    ) : null}
                    {params.InputProps.endAdornment}
                  </>
                ),
              }}
            />
          </FormControl>
        )}
      />
    </Tooltip>
  );
}
