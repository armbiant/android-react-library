'use client';

import Checkbox from '@mui/material/Checkbox';
import FormControl from '@mui/material/FormControl';
import FormControlLabel from '@mui/material/FormControlLabel';

import { useField } from 'formik';

export type CheckboxFieldProps = {
  /** The name of the form control to assign to this field */
  fcName: string;
  /**
   * The name of the field, displayed on the field's label
   *
   * @defaultValue ''
   */
  name?: string;
  /**
   * Whether or not this field is required in its form
   *
   * @defaultValue false
   */
  required?: boolean;
  /**
   * The width of the field
   *
   * @defaultValue 100%
   */
  width?: string;
  /**
   * The value of the field
   *
   * @defaultValue false
   */
  value?: boolean;
  /**
   * Whether or not this field is disabled
   *
   * @defaultValue false
   */
  disabled?: boolean;
  /**
   * Callback function to perform an action when the value changes
   *
   * @defaultValue null
   */
  onValueChange?: ((value: boolean) => void) | null;
};

const DEFAULT_PROPS: CheckboxFieldProps = {
  fcName: '',
  name: '',
  required: false,
  width: '100%',
  value: false,
  disabled: false,
  onValueChange: null,
};

export default function CheckboxField(props: CheckboxFieldProps) {
  props = {
    ...props,
    width: !props.width ? DEFAULT_PROPS.width : props.width,
    disabled: !props.disabled ? DEFAULT_PROPS.disabled : props.disabled,
    value: !props.value ? DEFAULT_PROPS.value : props.value,
  };

  const [field, meta, helpers] = useField({
    name: props.fcName,
    type: 'checkbox',
    checked: props.value,
  });

  const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    helpers.setValue(event.target.checked);

    if (props.onValueChange) {
      props.onValueChange(event.target.checked);
    }
  };

  return (
    <FormControl sx={{ mt: 1.5, width: props.width }}>
      <FormControlLabel
        label={props.name}
        control={
          <Checkbox
            color="primary"
            checked={field.value}
            onChange={handleChange}
            inputProps={{ 'aria-label': 'controlled' }}
          />
        }
      />
    </FormControl>
  );
}
