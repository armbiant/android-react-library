'use client';

import CheckIcon from '@mui/icons-material/Check';
import ErrorIcon from '@mui/icons-material/Error';
import CircularProgress from '@mui/material/CircularProgress';
import InputAdornment from '@mui/material/InputAdornment';

export type AdornmentProps = {
  /** The type of the field */
  type: 'text' | 'password';
  /** The position of the adornment */
  position: 'start' | 'end';
  /** A React Element to be displayed as an adornment */
  adornment?: JSX.Element;
  /** A React Element to be displayed as a start adornment */
  startAdornment?: JSX.Element | null;
  /** A React Element to be displayed as an end adornment */
  endAdornment?: JSX.Element | null;
};

/**
 * Adds a ReactElement to the beginning or end of a text field.
 *
 * TODO: example
 *
 * @param {AdornmentProps} props
 * @returns
 */
export function buildInputAdornmentProps(props: AdornmentProps) {
  const adornment = (
    <InputAdornment position={props.position}>{props.adornment}</InputAdornment>
  );

  return {
    type: props.type,
    position: props.position,
    startAdornment:
      props.adornment && props.position === 'start' ? adornment : null,
    endAdornment:
      props.adornment && props.position === 'end' ? adornment : null,
  };
}

const invalidInputAdornmentProps = buildInputAdornmentProps({
  type: 'text',
  position: 'end',
  adornment: <ErrorIcon sx={{ color: 'red' }} />,
});

const validInputAdornmentProps = buildInputAdornmentProps({
  type: 'text',
  position: 'end',
  adornment: <CheckIcon color="primary" />,
});

const validatingInputAdornmentProps = buildInputAdornmentProps({
  type: 'text',
  position: 'end',
  adornment: <CircularProgress color="primary" size={20} />,
});

export {
  invalidInputAdornmentProps,
  validInputAdornmentProps,
  validatingInputAdornmentProps,
};
