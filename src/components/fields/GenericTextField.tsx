'use client';

import FormControl from '@mui/material/FormControl';
import TextField from '@mui/material/TextField';
import Tooltip from '@mui/material/Tooltip';
import Zoom from '@mui/material/Zoom';

import { useField } from 'formik';
import { useState } from 'react';
import { KeyboardEventHandler } from 'react';
import { GenericFormItem } from '../../utils';
import { AdornmentProps } from './adornments';

export type GenericTextFieldProps = {
  /** The name of the form control to assign to this field */
  fcName: string;
  /**
   * The type of field
   *
   * @defaultValue 'text'
   */
  type: GenericFormItem['type'];
  /**
   * The name of the field, displayed on the field's label
   *
   * @defaultValue ''
   */
  name?: string;
  /**
   * The variant this field should be displayed as
   *
   * @defaultValue standard
   */
  variant?: 'standard' | 'outlined';
  /**
   * Callback function to perform an action when the value changes
   *
   * @defaultValue null
   */
  onValueChange?: ((event: any) => void) | null;
  /**
   * Callback function to perform an action when a key is pressed while this textbox is focused
   *
   * @defaultValue null
   */
  onKeyDown?: KeyboardEventHandler<HTMLDivElement> | null;
  /**
   * Whether or not this field is required in its form
   *
   * @defaultValue false
   */
  required?: boolean;
  /**
   * Input props as defined [here.](https://mui.com/material-ui/api/text-field/)
   * Provided as a way to define [adornments.](https://mui.com/material-ui/react-text-field/#input-adornments)
   *
   * @defaultValue "\{type: 'text'\}"
   */
  inputProps?: AdornmentProps;
  /**
   * The width of the field
   *
   * @defaultValue 100%
   */
  width?: string;
  /**
   * The value of the field
   *
   * @defaultValue ''
   */
  value?: string;
  /**
   * Whether or not this text field should allow for multiline input.
   *
   * @defaultValue false
   */
  multiLine?: boolean;
  /**
   * Whether or not this field is disabled
   *
   * @defaultValue false
   */
  disabled?: boolean;
  /**
   * Whether or not this field has an error
   *
   * @defaultValue false
   */
  error?: boolean;
  /**
   * The text of any errors
   *
   * @default null
   */
  errorMessage?: string | null;
  /**
   * The function that performs validations for this specific field
   *
   * @param value The value of the field
   * @returns An error message if the field is invalid, or undefined otherwise
   */
  validateFn?: (value: any) => string | undefined;
  /**
   * Whether or not this field is not part of a form
   *
   * @default false
   */
  standalone?: boolean;
  /**
   * Optional text to be displayed in a tooltip above the field
   *
   * @default '''
   */
  tooltip?: string;
};

const DEFAULT_PROPS: GenericTextFieldProps = {
  fcName: '',
  type: 'text',
  name: '',
  variant: 'standard',
  onValueChange: null,
  onKeyDown: null,
  required: false,
  inputProps: undefined,
  width: '100%',
  value: '',
  multiLine: false,
  disabled: false,
  error: false,
  errorMessage: null,
  standalone: false,
  tooltip: '',
};

/**
 * Generates a generic text field.
 * Based on [MUI Text Field.](https://mui.com/material-ui/react-text-field/)
 *
 * @param {GenericTextFieldProps} props
 * @example
 * import { GenericTextField } from '@oklahoma-biological-survey/obs-react-lib/nextjs/components';
 * import { useState } from 'react';
 *
 * export function TextField() {
 *   const [query, setQuery] = useState(null);
 *
 *   return (
 *     <GenericTextField fcName='example' variant='outlined' width='50%' onValueChange={setQuery} value={query} />
 *   );
 * }
 */
export default function GenericTextField(props: GenericTextFieldProps) {
  props = {
    ...props,
    variant: !props.variant ? DEFAULT_PROPS.variant : props.variant,
    inputProps: !props.inputProps ? DEFAULT_PROPS.inputProps : props.inputProps,
    width: !props.width ? DEFAULT_PROPS.width : props.width,
    error: !props.error ? DEFAULT_PROPS.error : props.error,
    errorMessage: !props.errorMessage
      ? DEFAULT_PROPS.errorMessage
      : props.errorMessage,
    multiLine: !props.multiLine ? DEFAULT_PROPS.multiLine : props.multiLine,
    disabled: !props.disabled ? DEFAULT_PROPS.disabled : props.disabled,
    value: !props.value ? DEFAULT_PROPS.value : props.value,
    tooltip: !props.tooltip ? DEFAULT_PROPS.tooltip : props.tooltip,
  };

  const [value, setValue] = props.standalone
    ? useState(props.value)
    : [undefined, undefined];
  const [field, meta, helpers] = props.standalone
    ? [undefined, undefined, undefined]
    : useField({
        name: props.fcName,
        value: props.value,
        validate: props.validateFn,
      });

  const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    const value = event.target.value;
    props.standalone ? setValue?.(value) : helpers?.setValue(value);

    if (props.onValueChange) {
      props.onValueChange(event);
    }
  };

  return (
    <>
      <Tooltip
        title={props.tooltip}
        placement="right"
        TransitionComponent={Zoom}
        enterDelay={500}
        enterNextDelay={100}
        arrow
      >
        <FormControl
          variant={props.variant}
          sx={{ mt: 1.5, width: props.width }}
        >
          <TextField
            id={`generic-field-${props.fcName}`}
            name={props.fcName}
            type={props.type}
            label={props.name}
            value={props.standalone ? value : field?.value}
            variant={props.variant}
            disabled={props.disabled}
            onChange={handleChange}
            onKeyDown={props.onKeyDown ? props.onKeyDown : undefined}
            error={props.error}
            helperText={props.errorMessage}
            inputProps={{
              ...field,
              // Ensure that the value of number fields are not changed on scroll
              ...{ onWheel: (e) => (e.target as HTMLElement).blur() },
            }}
            InputProps={props.inputProps}
            multiline={props.multiLine}
            rows={4}
          />
        </FormControl>
      </Tooltip>
      <br />
    </>
  );
}
