export type BasicUser = {
  id: string;
  firstName: string;
  lastName: string;
};
