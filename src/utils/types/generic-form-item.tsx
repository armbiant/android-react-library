import { AdornmentProps } from '../../components';

export type GenericFormItem = {
  fcName: string;
  name: string;
  type: 'custom' | 'date' | 'dropdown' | 'multiline' | 'number' | 'text';
  value?: any;
  tooltip?: string;
  // only used if type === 'dropdown'
  options?: Array<any>;
  // only used if type === 'dropdown'
  dropdownLabelFunctionPred?: (option: any) => string;
  // only used if type === 'dropdown'
  optionKey?: string;
  // only used if type === 'dropdown'
  optionValueKey?: string;
  // only used if type === 'dropdown'
  multiple?: boolean;
  required?: boolean;
  disabled?: boolean;
  inputProps?: Omit<AdornmentProps, 'adornment'>;
  error?: boolean;
  errorMessage?: string;
  // only used if type === 'custom'
  component?: JSX.Element;
  // whether or not this field is not part of a form. defaults to false
  standalone?: boolean;
  // only used if type === 'dropdown' and asynchronous searching should be enabled
  onInputChange?: (event: React.ChangeEvent<{}>, newValue: string) => void;
};
