import '@testing-library/jest-dom';
import React from 'react';
import SearchIcon from '@mui/icons-material/Search';

import { ChangeEvent } from 'react';
import { Formik } from 'formik';
import { GenericTextField, buildInputAdornmentProps } from '../../../src/components/fields';
import { fireEvent, render, screen } from '@testing-library/react';

describe('Generic Text Field', () => {
  it('renders a standard generic text field', () => {
    render(<Formik initialValues={{}} onSubmit={(values: {}) => console.log(values)}><GenericTextField name='Test' fcName='test' type='text' /></Formik>);

    const input = screen.getByLabelText(/test/i);

    expect(input.getAttribute('type') === 'text');
    expect(input.getAttribute('value') == '');
    expect(input.parentElement).toHaveClass('MuiInput-root');
  });

  it('renders an outlined generic text field', () => {
    render(<Formik initialValues={{}} onSubmit={(values: {}) => console.log(values)}><GenericTextField name='Test' fcName='test' type='text' variant='outlined' /></Formik>);

    const input = screen.getByLabelText(/test/i);
    expect(input.parentElement).toHaveClass('MuiOutlinedInput-root');
  });

  it('renders a required generic text field', () => {
    render(<Formik initialValues={{}} onSubmit={(values: {}) => console.log(values)}><GenericTextField name='Test' fcName='test' type='text' required={true} /></Formik>);

    const input = screen.getByLabelText(/test/i);
    expect(input.getAttribute('required') !== null);
  });

  it('renders a generic text field with a start adornment', () => {
    render(<Formik initialValues={{}} onSubmit={(values: {}) => console.log(values)}><GenericTextField name='Start Adornment' fcName='start-adornment' type='text' inputProps={buildInputAdornmentProps({type: 'text', position: 'start', adornment: ( <SearchIcon /> )})} /></Formik>);

    const input = screen.getByRole('textbox', {name: 'Start Adornment'});

    expect(input).toHaveClass('MuiInputBase-inputAdornedStart');
  });

  it('renders a generic text field with a provided value', () => {
    render(<Formik initialValues={{}} onSubmit={(values: {}) => console.log(values)}><GenericTextField name='Test' fcName='test' type='text' value='test value' /></Formik>);

    const input = screen.getByRole('textbox', {name: 'Test'});

    expect(input.getAttribute('value') === 'test value');
  });

  it('ensures that the value is updated on user input', () => {
    render(<Formik initialValues={{}} onSubmit={(values: {}) => console.log(values)}><GenericTextField name='Test' fcName='test' type='text' /></Formik>);

    const input = screen.getByLabelText(/test/i);

    expect(input.getAttribute('value')).toBe('');

    fireEvent.change(input, {target: {value: 'test value'}});

    expect(input.getAttribute('value')).toBe('test value')
  });

  it('ensures that the parent function is called', () => {
    let value = 'init';

    const valueChange = (event: ChangeEvent<any>) => value = event.target.value;

    render(<Formik initialValues={{}} onSubmit={(values: {}) => console.log(values)}><GenericTextField name='Test' fcName='test' type='text' onValueChange={valueChange} /></Formik>);

    const input = screen.getByLabelText(/test/i);

    expect(input.getAttribute('value')).toBe('');

    fireEvent.change(input, {target: {value: 'test value'}});

    expect(value).toEqual('test value');
  });

  it('renders a text field that listens for keystrokes', () => {
    let value = 'init';
    let enterKeyPressed = false;

    const valueChange = (event: ChangeEvent<any>) => value = event.target.value;

    const enterKey = (e: React.KeyboardEvent<HTMLInputElement>) => {
      if (e.key === 'Enter') {
        enterKeyPressed = true;
      }
    };

    render(<Formik initialValues={{}} onSubmit={(values: {}) => console.log(values)}><GenericTextField name='Test' fcName='test' type='text' onValueChange={valueChange} onKeyDown={enterKey} /></Formik>);

    const input = screen.getByLabelText(/test/i);

    expect(input.getAttribute('value')).toBe('');

    fireEvent.change(input, {target: {value: 'test value'}});
    fireEvent.keyDown(input, {key: 'Enter', code: 13, charCode: 13});

    expect(value).toEqual('test value');
    expect(enterKeyPressed).toBe(true);
  });

  it('renders a multiline text field', () => {
    render(<Formik initialValues={{}} onSubmit={(values: {}) => console.log(values)}><GenericTextField name='Test' fcName='test' type='text' multiLine={true} /></Formik>);

    const textbox = screen.getByRole('textbox', { name: 'Test' });

    expect(textbox).toBeInTheDocument();
  });
});
