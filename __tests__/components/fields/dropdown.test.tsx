import '@testing-library/jest-dom';
import React from 'react';

import { Formik } from 'formik';
import { GenericDropdown } from '../../../src/components/fields';
import { fireEvent, render, screen } from '@testing-library/react';

const options = [
  {
    id: 0,
    value: 'Option 1'
  },
  {
    id: 1,
    value: 'Option 2'
  },
  {
    id: 2,
    value: 'Option 3'
  }
];

describe('Generic Dropdown', () => {
  it('renders a generic dropdown', () => {
    render(<Formik initialValues={{}} onSubmit={(values: {}) => console.log(values)}><GenericDropdown fcName='test' name='Test' options={options} value={options.find(option => option.id === 0)} labelFunctionPred={(option: any) => option.value} optionKey='id' optionValueKey='value' /></Formik>);

    const input = screen.getByLabelText(/test/i);

    expect(input.getAttribute('type') === 'text');
    expect(input.getAttribute('value') == '');
    expect(input.parentElement).toHaveClass('MuiAutocomplete-inputRoot');
  });

  it('renders a generic dropdown with options', () => {
    render(<Formik initialValues={{}} onSubmit={(values: {}) => console.log(values)}><GenericDropdown fcName='test' name='Test' options={options} value={options.find(option => option.id === 0)} labelFunctionPred={(option: any) => option.value} optionKey='id' optionValueKey='value' /></Formik>);

    const openBtn = screen.getByRole('button', { name: 'Open' });

    fireEvent.click(openBtn);

    const optionList = screen.getAllByRole('option');

    expect(optionList.length).toBe(3);
  });

  it('renders an outlined generic dropdown', () => {
    render(<Formik initialValues={{}} onSubmit={(values: {}) => console.log(values)}><GenericDropdown fcName='test' name='Test' variant='outlined' options={options} value={options.find(option => option.id === 0)} labelFunctionPred={(option: any) => option.value} optionKey='id' optionValueKey='value' /></Formik>);

    const input = screen.getByLabelText(/test/i);
    expect(input.parentElement).toHaveClass('MuiOutlinedInput-root');
  });

  it('renders a required generic dropdown', () => {
    render(<Formik initialValues={{}} onSubmit={(values: {}) => console.log(values)}><GenericDropdown fcName='test' name='Test' required={true} options={options} value={options.find(option => option.id === 0)} labelFunctionPred={(option: any) => option.value} optionKey='id' optionValueKey='value' /></Formik>);

    const input = screen.getByLabelText(/test/i);
    expect(input.getAttribute('required') !== null);
  });
});
