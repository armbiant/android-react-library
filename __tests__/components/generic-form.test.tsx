import '@testing-library/jest-dom';
import CheckIcon from '@mui/icons-material/Check';
import React from 'react';

import { GenericForm } from '../../src/components';
import { GenericFormItem } from '../../src/utils';
import { act } from 'react-dom/test-utils';
import { render, screen } from '@testing-library/react';

const fields: Array<GenericFormItem> = [
  { fcName: 'field1', name: 'Field 1', type: 'text', value: 'value1' },
  { fcName: 'field2', name: 'Field 2', type: 'text', value: 'value2' },
  { fcName: 'field3', name: 'Field 3', type: 'text', value: 'value3' }
];

describe('Generic Form', () => {
  it('renders a generic form', async () => {
    let status = 'init';

    const onSubmit = async (values: any) => {
      status = 'submitted';
    };

    render(<GenericForm title='Test' fields={fields} onSubmit={onSubmit} submitBtnConfig={{ title: 'Submit Form', icon: <CheckIcon /> }} />);

    const heading = screen.getByRole('heading', {name: 'Test'});
    const fieldComponents = screen.getAllByRole('textbox');
    const submitBtn = screen.getByRole('button', {name: 'Submit Form'});

    expect(heading).toBeInTheDocument();
    expect(fieldComponents.length).toBe(3);
    expect(submitBtn).toBeInTheDocument();

    expect(status).toEqual('init');

    await act(() => {
      submitBtn.click();
    });

    expect(status).toEqual('submitted');
  });

  it('renders a generic form with a dropdown', () => {
    let status = 'init';

    const onSubmit = async (values: any) => {
      status = 'submitted';
    };

    const options1 = [
      {
        id: 0,
        value: 'Option 1',
      },
      {
        id: 1,
        value: 'Option 2'
      },
    ];

    const options2 = [
      {
        id: 0,
        value: 'Option 3',
      },
      {
        id: 1,
        value: 'Option 4'
      },
    ];

    fields.push(
      { fcName: 'field4', name: 'Field 4', type: 'dropdown', options: options1, value: options1.find(option => option.id === 0), dropdownLabelFunctionPred: (option: any) => option.value, optionKey: 'id', optionValueKey: 'value' },
      { fcName: 'field5', name: 'Field 5', type: 'dropdown', options: options2, value: options2.find(option => option.id === 0), dropdownLabelFunctionPred: (option: any) => option.value, optionKey: 'id', optionValueKey: 'value' },
    );

    render(<GenericForm title='Test' fields={fields} onSubmit={onSubmit} submitBtnConfig={{ title: 'Submit Form', icon: <CheckIcon /> }} />);

    const openBtns = screen.getAllByRole('button', {name: 'Open'})
    expect(openBtns.length).toBe(2);
  });

  it('renders a generic form with a multiline text field', () => {
    let status = 'init';

    const onSubmit = async (values: any) => {
      status = 'submitted';
    };

    fields.push(
      { fcName: 'field6', name: 'Field 6', type: 'multiline', value: '' }
    );

    render(<GenericForm title='Test' fields={fields} onSubmit={onSubmit} submitBtnConfig={{ title: 'Submit Form', icon: <CheckIcon /> }} />);

    const textbox = screen.getByRole('textbox', { name: 'Field 6' });

    expect(textbox).toBeInTheDocument();
  })
});
