import '@testing-library/jest-dom';
import React from 'react';

import { ConfirmDialog } from '../../src/components';
import { act } from 'react-dom/test-utils';
import { render, screen } from '@testing-library/react';

describe('Confirm Dialog', () => {
  const messageText = 'Test Message';
  let status = 'init';

  const handleCancel = () => {
    status = 'cancelled';
  };

  const handleConfirm = () => {
    status = 'confirmed';
  };

  it('renders a confirmation dialog', async () => {
    render(<ConfirmDialog open={true} message={messageText} onCancel={handleCancel} onConfirm={handleConfirm} />);

    const dialog = screen.getByRole('dialog', { name: 'Confirm' });
    const message = screen.getByText(messageText);
    const cancelBtn = screen.getByRole('button', { name: 'Cancel' });
    const confirmBtn = screen.getByRole('button', { name: 'Confirm' });

    expect(dialog).toBeInTheDocument();
    expect(message).toBeInTheDocument();
    expect(cancelBtn).toBeInTheDocument();
    expect(confirmBtn).toBeInTheDocument();

    expect(status).toBe('init');

    await act(() => {
      cancelBtn.click();
    });

    expect(status).toBe('cancelled');
    status = 'init';

    expect(status).toBe('init');

    await act(() => {
      confirmBtn.click();
    });

    expect(status).toBe('confirmed');
  });
});
