import '@testing-library/jest-dom';
import React from 'react';

import { NavbarExpandOptions } from '../../../src/components/nav/NavbarExpandOptions';
import { Route } from '../../../src/utils';
import { act } from 'react-dom/test-utils';
import { createTheme } from '@mui/material/styles';
import { render, screen } from '@testing-library/react';

const theme = createTheme();

const routes: Array<Route> = [
  {
    name: 'route-1',
    paths: ['path-1'],
    options: [
      {
        name: 'option-1',
        paths: ['option-path-1']
      },
      {
        name: 'option-2',
        paths: ['option-path-2']
      }
    ]
  }
]

describe('Collapsed Navbar Expansion', () => {
  it('renders an expansion panel when the navbar is collapsed due to being on a small screen', () => {
    render(<NavbarExpandOptions routes={routes} index={0} theme={theme} />);

    const routeBtn = screen.getByRole('button', { name: 'route-1' });

    expect(routeBtn).toBeInTheDocument();

    act(() => {
      routeBtn.click();
    });

    const optionOneBtn = screen.getByRole('button', { name: 'option-1' });
    const optionTwoBtn = screen.getByRole('button', { name: 'option-2' });

    expect(optionOneBtn).toBeInTheDocument();
    expect(optionTwoBtn).toBeInTheDocument();
  });

  it('renders nothing', () => {
    render(<NavbarExpandOptions routes={routes} index={undefined} theme={theme} />);

    expect(() => screen.getAllByRole('button')).toThrowError('There are no accessible roles.');
  });
});
